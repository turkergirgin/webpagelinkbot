﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace WebPageLinkBot
{
    class Program
    {
        public static string ParametreURL;
        public static string KontrolURL;
        public static string SaveFileName; // = "LinkListFile.txt";
        public static bool ImageLink = true;
        public static bool CSSLink = true;
        public static bool XMLLink = true;
        public static bool JSONLink = true;
        public static bool PDFLink = true;
        public static bool AllSubPage = false;
        public static bool SHOWLink = false;
        public static bool SHOWTotalLink = false;
        public static bool HELPCode = false;
        public static bool SaveLink = false;
        public static List<string> ALLLinkList = new List<string>();
        public static List<string> SUBLinkList = new List<string>();
        public static int PageCounter = 0;
        public static int ErrorCounter = 0;
        public static int SaveLinkParamNumber = 0;

        static void Main(string[] args)
        {
            //Parametre girilip girilmediği kontrol ediliyor.
            if (args.Length <= 0)
            {
                Console.WriteLine("Please enter a URL as parameter, for example : 'WebPageLinkBot https://www.google.com'");
                Console.WriteLine("or for a complete example : 'WebPageLinkBot https://www.google.com -image -css -xml -json -pdf -all -show -showlist -save axamplelist.txt'");
                Console.WriteLine("Use the '-h' parameter to get help");
            }
            else
            {
                //Girilen parametreler kontrol edilerek ilgili değişken false yada true olarak değiştirirlir
                for (int z = 0; z < args.Length; z++)
                {
                    if (args[z].ToLower() == "-image" || args[z].ToLower() == "-ımage") ImageLink = false;
                    if (args[z].ToLower() == "-css") CSSLink = false;
                    if (args[z].ToLower() == "-xml") XMLLink = false;
                    if (args[z].ToLower() == "-json") JSONLink = false;
                    if (args[z].ToLower() == "-pdf") PDFLink = false;
                    if (args[z].ToLower() == "-all") AllSubPage = true;
                    if (args[z].ToLower() == "-show") SHOWLink = true;
                    if (args[z].ToLower() == "-showlist" || args[z].ToLower() == "-showlıst") SHOWTotalLink = true;
                    if (args[z].ToLower() == "-h" || args[z].ToLower() == "-help") HELPCode = true;
                    if (args[z].ToLower() == "-save")
                    {
                        SaveLink = true;
                        SaveFileName = args[0] + DateTime.Now.ToString();
                        SaveFileName = SaveFileName.Replace("://","_");
                        SaveFileName = SaveFileName.Replace(":", "_");
                        SaveFileName = SaveFileName.Replace("/", "_");
                        SaveFileName = SaveFileName.Replace(".", "_");
                        SaveFileName = SaveFileName.Replace(" ", "_");
                        SaveFileName = SaveFileName + ".txt";
                        SaveLinkParamNumber = z + 1;
                    }
                }

                if (SaveLinkParamNumber > 0 && args.Length > SaveLinkParamNumber) SaveFileName = args[SaveLinkParamNumber].ToString();

                if (HELPCode)
                {
                    Console.WriteLine();
                    Console.WriteLine("WebPageLinkBot 1.00 (.Net Core 2.1) : Copyright (c) 2018 Türker Girgin  : MIT License ");
                    Console.WriteLine();
                    Console.WriteLine("Usage: WebPageLinkBot <URL> [<parameters>...] <link_list_file_name>");
                    Console.WriteLine();
                    Console.WriteLine("<Parameters>");
                    Console.WriteLine("-image : pass image files used as links");
                    Console.WriteLine("-css : pass css files used as links");
                    Console.WriteLine("-xml : pass xml files used as links");
                    Console.WriteLine("-json : pass json files used as links");
                    Console.WriteLine("-pdf : pass json files used as links");
                    Console.WriteLine("-all : include review of all sub-pages");
                    Console.WriteLine("-show : view links you find on every page");
                    Console.WriteLine("-showlist : show all links at the end of the review as a list");
                    Console.WriteLine("-h : view help documentation");
                    Console.WriteLine("-save : save to file all links at the end of the review as a list");
                    Console.WriteLine("-save <link_list_file_name> : save to file all links at the end of the review as a list with the specified file name");

                    Environment.Exit(0);
                }

                ParametreURL = args[0].ToLower();

                //Parametre olarak girilen URL nin sonunda varsa / siliniyor.
                if (ParametreURL.EndsWith("/"))
                {
                    ParametreURL = ParametreURL.Substring(0, ParametreURL.Length - 1);
                }

                KontrolURL = ParametreURL;
                KontrolURL = KontrolURL.Replace("https://","");
                KontrolURL = KontrolURL.Replace("http://", "");
                KontrolURL = KontrolURL.Replace("www.", "");

                string AllHtml = DownloadHTML(ParametreURL);

                if (AllHtml != "Error")
                {
                    //Parametre olarak girilen URL den dönen sonuçlar listeye toplu alarak ekleniyor
                    ALLLinkList = LinkFinder(AllHtml);

                    PageCounter++;
                }
                else
                {
                    Console.WriteLine("Error: Check your entered URL or internet connection!");
                    ErrorCounter++;
                }

                if (AllSubPage)
                {
                    while (PageCounter + ErrorCounter <= ALLLinkList.Count)
                    {

                        AllHtml = DownloadHTML(ALLLinkList[(PageCounter + ErrorCounter) - 1]);
                        //Console.WriteLine(ALLLinkList[(PageCounter + ErrorCounter) - 1] + " içinde " + KontrolURL + "bulundu");

                        if (AllHtml != "Error")
                        {
                            SUBLinkList = LinkFinder(AllHtml);

                            //Listeye dönen sonuçları tek tek ekleme
                            //Tam burada alt sayfalarda bulunan linklerde gerçek listeye ekleniyor.
                            for (int i = 0; i < SUBLinkList.Count; i++)
                            {
                                if (!ALLLinkList.Contains(SUBLinkList[i]))
                                {
                                    ALLLinkList.Add(SUBLinkList[i]);
                                }
                            }

                            PageCounter++;
                        }
                        else
                        {
                            Console.WriteLine("Error: Incorrect URL or Check your internet connection!");
                            ErrorCounter++;
                        }

                    }
                }

                Console.WriteLine();

                //Parametre girildiyse bulunan tüm linkler ekrana listeleniyor.
                if (SHOWTotalLink)
                {
                    for (int t = 0; t < ALLLinkList.Count; t++)
                    {
                        Console.WriteLine(ALLLinkList[t]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("Total Broken Link: " + ErrorCounter.ToString());
                Console.WriteLine("Total Page: " + PageCounter.ToString());
                Console.WriteLine("Total Link: " + ALLLinkList.Count.ToString());

                //Parametre girildiyse bulunan tüm linkler dosyaya kaydediliyor..
                if (SaveLink)
                {
                    try
                    {
                        Console.WriteLine();
                        Console.Write("[" + SaveFileName + "] " + " -> is being saved ");
                        System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(SaveFileName);
                        SaveFile.WriteLine("Link List for " + ParametreURL);
                        SaveFile.Write("Used parameters:");
                        for (int zz = 1; zz < args.Length; zz++)
                        {
                            SaveFile.Write(" " + args[zz]);
                        }
                        SaveFile.WriteLine();
                        SaveFile.WriteLine();
                        for (int s = 0; s < ALLLinkList.Count; s++)
                        {
                            SaveFile.WriteLine(ALLLinkList[s]);
                            if (s % 50 == 0) Console.Write(".");
                        }
                        SaveFile.WriteLine();
                        SaveFile.WriteLine("Total Broken Link: " + ErrorCounter.ToString());
                        SaveFile.WriteLine("Total Page: " + PageCounter.ToString());
                        SaveFile.WriteLine("Total Link: " + ALLLinkList.Count.ToString());
                        SaveFile.Close();
                        Console.WriteLine(" -> Done!");
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Can not write the file, please check your permissions.");
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }

        private static string DownloadHTML(string url)
        {
            if (url.Contains(KontrolURL))
            {
                StringBuilder sb = new StringBuilder();
                byte[] buf = new byte[8192];
                try
                {
                    Console.Write("[" + (PageCounter + ErrorCounter).ToString() + "/" + ALLLinkList.Count.ToString() + "] " + url + " -> Downloading ");
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream resStream = response.GetResponseStream();
                    string tempString = null;
                    int count = 0;
                    do
                    {
                        count = resStream.Read(buf, 0, buf.Length);
                        if (count != 0)
                        {
                            tempString = Encoding.UTF8.GetString(buf, 0, count);
                            sb.Append(tempString);
                            Console.Write(".");
                        }
                    }
                    while (count > 0); // any more data to read?
                    Console.WriteLine(" -> OK!");
                    return sb.ToString();
                }
                catch (Exception)
                {
                    return "Error";
                }
            }
            else
            {
                Console.WriteLine("[" + (PageCounter + ErrorCounter).ToString() + "/" + ALLLinkList.Count.ToString() + "] " + url + " -> Not Subdomain! ");
                return "";
            }
 
        }

        private static List<string> LinkFinder(string PageHTMLCode)
        {
            List<string> Linkler = new List<string>();
            string FindedLink;

            while (PageHTMLCode.Contains(@"href="""))
            {
                //ilk bulunan href=" dahil olacak şekilde öncesi değişkenden sökülüyor
                PageHTMLCode = PageHTMLCode.Remove(0, PageHTMLCode.IndexOf(@"href=""") + 6);
                //ilk bulunan " işaretinden öncesi değişkene atılıyor.
                FindedLink = PageHTMLCode.Substring(0, PageHTMLCode.IndexOf(@""""));
                //Link içinde bulunabilecek yeni satır karakterlerini ve satırbaşı satır sonu boşluklarını kırpalım
                FindedLink = FindedLink.Replace(Environment.NewLine, " ").Trim();

                // bulunan değer "" yada "/" yada "#" ise es geçiliyor
                if (FindedLink == "" || FindedLink =="/" || FindedLink == "#")
                {
                    continue;
                }

                //bulunan değer javascript: ile başlıyorsa es geçiliyor
                if (FindedLink.StartsWith("javascript:"))
                {
                    continue;
                }

                //bulunan değer mailto: ile başlıyorsa es geçiliyor
                if (FindedLink.StartsWith("mailto:"))
                {
                    continue;
                }

                //bulunan değer tel: ile başlıyorsa es geçiliyor
                if (FindedLink.StartsWith("tel:"))
                {
                    continue;
                }

                //Parametre girildiyse image dosyalarına ait likler es geçiliyor
                if (!ImageLink)
                {
                    if (FindedLink.ToLower().Contains(".ico") || FindedLink.ToLower().Contains(".jpg") || 
                        FindedLink.ToLower().Contains(".jpeg") || FindedLink.ToLower().Contains(".png") ||
                        FindedLink.ToLower().Contains(".gif") || FindedLink.ToLower().Contains(".svg"))
                    {
                        continue;
                    }
                }

                //Parametre girildiyse css dosyalarına ait likler es geçiliyor
                if (!CSSLink)
                {
                    if (FindedLink.ToLower().Contains(".css"))
                    {
                        continue;
                    }
                }

                //Parametre girildiyse xml dosyalarına ait likler es geçiliyor
                if (!XMLLink)
                {
                    if (FindedLink.ToLower().Contains(".xml"))
                    {
                        continue;
                    }
                }

                //Parametre girildiyse json dosyalarına ait likler es geçiliyor
                if (!JSONLink)
                {
                    if (FindedLink.ToLower().Contains(".json"))
                    {
                        continue;
                    }
                }

                //Parametre girildiyse pdf dosyalarına ait likler es geçiliyor
                if (!PDFLink)
                {
                    if (FindedLink.ToLower().Contains(".pdf"))
                    {
                        continue;
                    }
                }

                //eğer bulunan link / ile başlıyorsa girilen url başa eklenir
                if (FindedLink.StartsWith("/"))
                {
                    FindedLink = ParametreURL + FindedLink;
                }

                //eğer bulunan link # yada ? ile başlıyorsa girilen url başa eklenir araya / konur
                if (FindedLink.StartsWith("#") || FindedLink.StartsWith("?"))
                {
                    FindedLink = ParametreURL + "/" + FindedLink;
                }

                if (!FindedLink.Contains("www.") && !FindedLink.Contains("http"))
                {
                    FindedLink = ParametreURL + "/" + FindedLink;
                }

                //listede aynı değer yoksa ekleniyor.
                if (!Linkler.Contains(FindedLink))
                {
                    Linkler.Add(FindedLink);
                }

            }

            //Parametre girildiyse bulunan likler ekrana listeleniyor.
            if (SHOWLink)
            {
                for (int x = 0; x < Linkler.Count; x++)
                {
                    Console.WriteLine(Linkler[x]);
                }
            }
 
            return Linkler;
        }

    }
}
